terraform {
  required_providers {
    ignition = {
      source = "community-terraform-providers/ignition"
      version = "2.2.1"
    }
    libvirt = {
      source = "dmacvicar/libvirt"
      version = "0.7.4"
    }
  }
}

provider "libvirt" {
  uri = var.qemu_uri
}
