variable "instances" {
  type = list(any)
}

variable "qemu_uri" {
  type = string
  description = "URI to the libvirt socket, qemu:///session does not work on Fedora and it must be qemu:///session&socket=/run/user/UID/libvirt/virtqemud-sock"
}
