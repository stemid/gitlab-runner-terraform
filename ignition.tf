module "ignition" {
  count = length(var.instances)
  source = "git::https://gitlab.com/stemid/coreos-base-ignition.git"
  instance = {
    name = var.instances[count.index].name
    network = {
      method = "auto"
      interface = ""
    }
    serviceusers = var.instances[count.index].serviceusers
    node_exporter = var.instances[count.index].node_exporter
  }
}

module "gitlab_runner" {
  count = length(var.instances)
  source = "git::https://gitlab.com/stemid/gitlab-runner-ignition.git"
  home = "/home/gitlab"
  uid = 1010
  gid = 1010
  runners = var.instances[count.index].gitlab_runners
}

data "ignition_config" "config" {
  count = length(var.instances)
  users = [for m in flatten(module.ignition[count.index].ignition_user) : m.rendered]
  files = [for m in flatten(module.ignition[count.index].ignition_file) : m.rendered]
  links = [for m in flatten(module.ignition[count.index].ignition_link) : m.rendered]
  systemd = [
    for m in flatten(module.ignition[count.index].ignition_systemd_unit) : m.rendered
  ]
  directories = [
    for m in flatten(module.ignition[count.index].ignition_directory) : m.rendered
  ]
  #merge {
  #  source = "data:text/plain;base64,${base64encode(module.ignition[count.index].config.rendered)}"
  #}
  merge {
    source = "data:text/plain;base64,${base64encode(module.gitlab_runner[count.index].config.rendered)}"
  }
}
