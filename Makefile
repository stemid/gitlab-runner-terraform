PLAN?=current-plan
sshKey=~/.ssh/id_terraform-gitlab-runner_ed25519

all: init plan apply output

upgrade:
	terraform init -upgrade

init:
	terraform init

plan: init
	terraform plan -out="${PLAN}"

apply:
	terraform apply "${PLAN}"

output:
	terraform output -raw sshkey > ${sshKey}
	chmod 0600 ${sshKey}

providers:
	terraform providers

destroy:
	terraform apply -destroy -auto-approve
	test -f ${sshKey} && rm -v ${sshKey}
