module "vm" {
  count = length(var.instances)
  source = "git::https://gitlab.com/stemid/libvirt-vm-terraform.git"
  instance = var.instances[count.index]
  ignition_config = data.ignition_config.config[count.index].rendered
}
