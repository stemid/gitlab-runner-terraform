output "json" {
  value = data.ignition_config.config[0].rendered
  sensitive = true
}

output "sshkey" {
  value = module.ignition[0].sshkey.private_key_openssh
  sensitive = true
}

output "ip" {
  value = module.vm[*]
}
