# Gitlab runner Terraform

This is a Terraform module used by me personally to setup gitlab runners.

# Example how to

This can serve as a little introduction to Terraform that you can run at home against a libvirt host.

## Step 1: Create a terraform.tfvars file

This file contains all the input for the module, including secrets. So don't commit it to git repos.

Here is a basic minimal example, with fake secret values.

```hcl
instances = [
  {
    name = "gitlab-runner01"
    vcpu = 2
    memory = 2048
    image = "/home/stemid/.local/share/libvirt/images/fedora-coreos-38.20231014.2.0-qemu.x86_64.qcow2"
    network = {
      name = "default"
      mode = "bridge"
      bridge = "virbr0"
      addresses = ["192.168.122.122"]
      nameservers = ["192.168.122.1"]
    }
    serviceusers = [
      {
        username = "gitlab"
        home = "/home/gitlab"
        uid = 1010
        gid = 1010
      },
      {
        username = "node_exporter"
        home = "/home/node_exporter"
        uid = 1011
        gid = 1011
        publish_host = "0.0.0.0"
      }
    ]
    node_exporter = {
      home = "/home/node_exporter"
      uid = 1011
      gid = 1011
      publish_host = "0.0.0.0"
    }
    gitlab_runners = [
      {
        name = "first-runner"
        url = "https://gitlab.com"
        token = "secret-runner-token"
      },
      {
        name = "my-second-runner"
        url = "https://gitlab.com"
        token = "xXx-secret-runner-token-xXx"
      },
    ]
  }
]
```

This defines one Virtual Machine, with two gitlab runners. It uses a QEMU image you can [download from the Fedora CoreOS project website](https://fedoraproject.org/coreos/download?stream=testing#arches), and place into an available pool in your libvirt setup.

## Step 2: Configure the connection to your libvirt service

The file ``providers.tf`` contains configuration for the [libvirt provider](https://registry.terraform.io/providers/dmacvicar/libvirt/latest/docs), so take a quick look at that.

Here you can see that I'm setting the uri config option to ``var.qemu_uri`` which is an input variable. So how do we define it? I suggest using your environment.

```
export TF_VAR_qemu_uri=qemu+ssh://user@host/system
```

All environment variables that start with ``TF_VAR_`` are automatically used as input variables in Terraform. That way you don't have to define sensitive stuff in a file.

>In real life scenarios I use [direnv](https://github.com/direnv/direnv) to set and manage such environment variables with one .envrc file per git repo. Never commit your .envrc file, keep it in a password store.

## Step 3: Run Terraform

Normally you'd run the command terraform, but I've provided a Makefile for convenience. You still need to [install the terraform binary](https://developer.hashicorp.com/terraform/tutorials/aws-get-started/install-cli#install-terraform), and you also need to install GNU Make.

>On Fedora systems you can install GNU make by running ``sudo dnf install make``.

Now it's enough to just run ``make`` and it will create the VM for you.

```
make
... lots of output ...
Apply complete! Resources: 6 added, 0 changed, 0 destroyed.
```

This command does a few things;

* Runs ``terraform init``.
* Runs ``terraform plan``.
* Runs ``terraform apply``.
* Runs ``terraform output`` to output the installed SSH key into ``~/.ssh/id_terraform-gitlab-runner_ed25519``.
* Runs ``chmod 0600`` on the ssh key.

## Step 4: Destroy the resources

Everything you create with Terraform is kept in a state, so everything kept in that state can also be destroyed.

```
make destroy
```

This command does the following;

* Runs ``terraform destroy``.
* Removes the previously created SSH key.

## Caveats

I use a network bridge called virbr0 that must already exist. In the case of Fedora Workstation this bridge gets created automatically when you connect to the ``qemu:///system`` in virt-manager, and provide your user password to authenticate.

If you're running a server, hopefully this bridge is already defined for use with other VMs.
